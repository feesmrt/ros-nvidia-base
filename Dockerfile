# feesmrt/ros-nvidia-base
# Docker project for creating a ROS image with full indigo desktop installation.
# The nvidia-docker interface is used to get hardware acceleration.
# Root image is the osrf/ros:indigo-desktop-full
# Updates & Upgrades osrf/ros-indigo-desktop-full package
# Adds rosuser to enable build process within home directory

# ROS indigo with ubuntu 14.04
FROM osrf/ros:indigo-desktop-full

MAINTAINER feesmrt

# Config image to run with nvidia-docker
LABEL com.nvidia.volumes.needed="nvidia_driver"
ENV PATH /usr/local/nvidia/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64:${LD_LIBRARY_PATH}

# Create user: rosuser | set password: rosuser | add user to sudoers
RUN useradd -c "ROS standard user" -m -s /bin/bash rosuser \
    && /bin/bash -c "echo rosuser:rosuser | chpasswd" \
    && /bin/bash -c "usermod -a -G sudo rosuser" \
    && echo "rosuser ALL=NOPASSWD: ALL" >> /etc/sudoers

# Register user to docker
USER rosuser
WORKDIR /home/rosuser

# Update | Upgrade | Dist-Upgrade | Autoremove
RUN sudo apt-get -y update \
    && sudo apt-get -y upgrade \
    && sudo apt-get -y dist-upgrade \
    && sudo apt-get -y autoremove

# Install tools
RUN sudo apt-get -y install nano

# Source setup bash
RUN echo "source /opt/ros/indigo/setup.bash" >> /home/rosuser/.bashrc
