#!/bin/bash
###############################################################################
#* docker-control
#*
#* This script is used to simplify the start, exec and cleanup of docker images
#*
#* License BSD - Martin Fees - 2016 (see main directory for full license)
#*******************************************************************************

# Variables for names
VERSION=1.0
AUTHOR="Martin Fees"
EMAIL="martin.fees@gmx.de"

# Docker image
DOCKER_IMAGE=feesmrt/ros-nvidia-base:indigo

#*******************************************************************************

#
# Definition of function append ()
#
# brief: Append a new bash to the running image

append ()
{
  # Get the latest started and running container
  CONTAINER=$(docker ps | grep -Eo "^[0-9a-z]{8,}\b")

  echo "***********************************************************************"
  echo "* Appending bash to container"
  echo "***********************************************************************"
  echo "* Starting bash: "
  echo "***********************************************************************"

  docker exec -it $CONTAINER bash

  echo "***********************************************************************"
  echo "* Shutdown"
  echo "***********************************************************************"
}

#*******************************************************************************

#
# Definition of function cleanup ()
#
# brief: Delet runned scripts

cleanup ()
{
  while true; do
      echo "***********************************************************************"
      echo "Do you wish to cleanup stopped docker images (y/n)?"
      echo "***********************************************************************"
      read -p "(y/n): " yn
      case $yn in
          [Yy]* )
            echo "***********************************************************************"
            echo "Cleanup runned images"
            echo "***********************************************************************"
            docker rm $(docker ps -a -q -f status=exited);
            echo "***********************************************************************"
            break;;
          [Nn]* ) break;;
          * ) echo "Please answer yes or no.";;
      esac
  done
}

#*******************************************************************************

#
# Definition of function show_help ()
#
# brief: Functions prints information about the script

show_help ()
{
  echo "***********************************************************************"
  echo "* $DOCKER_IMAGE - Script to control docker images"
  echo "*"
  echo "* Commands:"
  echo "* -a <ID> | --append : Appends a bash to the started image"
  echo "* -c      | --cleanup: Cleanup all runned images"
  echo "* -h      | --help   : Prints help"
  echo "* -s      | --start  : Starting the image $DOCKER_IMAGE"
  echo "-----------------------------------------------------------------------"
  echo "*"
  echo "* Arguments:"
  echo "* -im     | --image     : Define a new default image "
  echo "*         |             : Parameter: <image_name>"
  echo "* -nv     | --nvidia    : Starts the container with nvidia support"
  echo "* -vo     | --volume    : Mounts a host pc folder to an image folder"
  echo "*                       : Parameter: <host_path>:<image_path>"
  echo "* -ws     | --workspace : Mounts a host pc folder to the catkin_ws"
  echo "*                         of the image"
  echo "*                       : Parameter: <host_path>"
  echo "***********************************************************************"
}

#*******************************************************************************

#
# Definition of function start ()
#
# brief: Function starts the docker image with bash

start ()
{
  # Check Arguments
  ARG_HARDWARE_ACCEL=0
  ARG_WS="";
  ARG_VOL="";
  NUM_VOLUMES=0

  echo "***********************************************************************"
  echo "* Starting Docker image                : $DOCKER_IMAGE"

  while :; do
    case $1 in
      -s|--start)
      ;;
      -c|--cleanup)
      ;;
      -h|-\?|--help)
      ;;
      -s|--start)
      ;;
      -nv|--nvidia)
      echo "-----------------------------------------------------------------------"
      echo "* Nvidia Hardware Acclleration         : enabled"

      ARG_HARDWARE_ACCEL=1
      ;;
      -im|--image)
      echo "-----------------------------------------------------------------------"
      echo "* Using default image                  : $2"
      DOCKER_IMAGE=$2
      shift
      ;;
      -vo|--volume)
      #if [ -d "$2" ]; then
        echo "-----------------------------------------------------------------------"
        echo "* Mounting host folder to image folder:"
        echo "* $2"
        ARG_VOL="$ARG_VOL --volume=$2"
        shift
      #else
      #  echo "***********************************************************************"
      #  printf'Error: "-vo|--volume" $2 directory does not exist!\n' >&2
      #  echo "***********************************************************************"
      #  exit 7
      #fi
      ;;
      -ws|--workspace)
        if [ -d "$2" ]; then
          if [ $NUM_VOLUMES -lt "1" ]; then
            echo "-----------------------------------------------------------------------"
            echo "* Mounting host workspace to image workspace:"
            echo "* $2:/home/rosuser/catkin_ws"
            ARG_WS="--volume=$2:/home/rosuser/catkin_ws"
            NUM_VOLUMES=$(($NUM_VOLUMES + 1))
            shift
          else
            echo "***********************************************************************"
            printf 'Error: "-ws|--workspace" Only one volume can be mounted!\n' >&2
            echo "***********************************************************************"
            exit 4
          fi
        else
          echo "***********************************************************************"
          printf 'Error: "-ws|--workspace" $2 directory does not exist!\n' >&2
          echo "***********************************************************************"
          exit 5
        fi
      ;;
      -?*)
      echo "***********************************************************************"
      printf 'WARN: Unknown option (ignored): %s\n' "$2" >&2
      echo "***********************************************************************"
      exit 6
      ;;
      *)
      break
    esac
    shift
  done

  echo "***********************************************************************"
  echo "* Connect a new window to this container with: $0 -a"
  echo "***********************************************************************"

  # Starting
  COMMAND=""
  if [ $ARG_HARDWARE_ACCEL == "1" ]; then
    # generate command
    COMMAND="nvidia-docker run -it"
  else
    # generate command
    COMMAND="docker run -it"
  fi

  # RUN COMMAND
  $COMMAND \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    $ARG_WS \
    $ARG_VOL \
    $DOCKER_IMAGE \
    bash

  echo "***********************************************************************"
  echo "* Shutdown"
  echo "***********************************************************************"
}

#*******************************************************************************


#
# Main script function
#
# brief: Runs the script

echo "***********************************************************************"
echo "* DOCKER CONTROL SCRIPT"
echo "-----------------------------------------------------------------------"
echo "* Version: $VERSION"
echo "* Author : $AUTHOR"
echo "* E-Mail : $EMAIL"
echo "* License: BSD"
echo "***********************************************************************"

# Check if there are enough Arguments
if [ "$#" -lt 1 ]; then
  echo "***********************************************************************"
  echo "Error: Not enough arguments!"
  echo "***********************************************************************"
  show_help
  exit 1
fi
# First argument has to be the command
case $1 in
  -a|--append)    # Calls the append function
    append
    exit
    ;;
  -c|--cleanup)   # Calls the cleanup function
    cleanup
    exit
    ;;
  -h|-\?|--help)  # Calls the help function an shows information about script
    show_help
    exit
    ;;
  -s|--start)    # Calls the start function
    start "$@"
    exit
    ;;
  -?*)
    echo "***********************************************************************"
    printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
    echo "***********************************************************************"
    ;;
    *)
    break
esac
